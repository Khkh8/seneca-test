// basic sum

let seneca = require('seneca')({log:'silent'})

// seneca.add
// - pattern: property pattern to match
// - action: function to be called when property match
seneca.add('role:math,cmd:sum', (msg, reply) => {
    let sum = msg.left + msg.right
    reply(null, {answer: sum})
})

// seneca.act
// - msg: inbound message
// - respond: cb function to be called on reply
seneca.act({role:'math', cmd:'sum', left: 4, right: 10}, (err, result) => {
    if (err) console.log('Action callback error: ', err)
    console.log('Sum: ', result);
})